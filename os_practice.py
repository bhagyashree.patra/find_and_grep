#!/usr/bin/env python3

import os
import sys

# print(os.path.abspath(__file__))
# print(os.path.abspath(os.curdir)) # current directory
print(os.path.basename(__file__))
# print(os.path.dirname(__file__))
# print(os.path.split(__file__))
print(os.path.splitext(__file__))
print(os.path.splitext(os.path.basename(__file__))[0])
# print(os.path.exists(__file__))
# print(os.path.isfile(__file__))
# print(os.path.isdir(__file__))
# print(os.path.islink(__file__))
# print(os.path.getsize(__file__))
# print(os.path.getmtime(__file__)) # last modified time
# print(os.path.getatime(__file__)) # last accessed time
# print(os.path.getctime(__file__)) # creation time

# print(sys.platform)
# print(sys.version)
# print(sys.path)
# print(sys.stdin,'\n',sys.stdout,'\n',sys.stderr)
# print(sys.argv)
# print(sys.argv[0])
# print(sys.argv[1])
# print(sys.argv[-1])
# print(sys.argv[-2])
# print(len(sys.argv))

# print(os.path.basename(sys.argv[0]))



