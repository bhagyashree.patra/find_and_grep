#!/bin/bash

if [ $# -ne 1 ] ; then
  echo "Usage: $0 <x.y.z>" 1>&2
  exit 1
fi
