#!/bin/bash

for text_file in `find . -name '*.txt'`
do
    echo $text_file
    head -n 1 $text_file 1> /dev/null
    echo
done