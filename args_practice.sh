#!/bin/bash

# Variable assignment
NumberOfArgs=$#

echo "Number of args: $NumberOfArgs"

# Print the index of each argument
count=1
while [ $count -le $NumberOfArgs ]
do
  echo "Argument $count"
  #count=count+1
  count=$(( $count + 1 ))
done

# Print each argument
for arg in "$@"
do
    echo "Arg: $arg"
done

exit 0

echo $#
echo $*
echo $@
echo "$@"
